This is an updated version of the VolumeSlicer macro (http://doube.org/files/VolumeSlicer.txt). It is intended to work with Fiji (http://fiji.sc/Fiji).

## Changelog
* 11/08/2015: Added a step-by-step description describing how to use this macro
* 03/03/2015: Initial release

## Documentation

Here are the exact steps to render a single image with the default parameters:

1.) Import an image sequence (for example a volume image sample sequence taken from here: https://graphics.stanford.edu/data/voldata/mrbrain-16bit.tar.gz) via "File->Import->Image Sequence" in Fiji.

2.) Copy the exact code from here (https://gitlab.com/lukasbrausch/volume_slicer_new/blob/master/volume_slicer.txt) to a file on your local machine and call it (for example) volume_slicer.txt. Please note that you have to copy the whole text. "Save page as..." won't do the trick.

3.) Go to "Plugins->Macros->Run and chose the file "volume_slicer.txt.

4.) After that just click "ok" and a single image with the default parameters will render.

This macro was tested on a Trisquel GNU/Linux system. In case you have any problems running the code, please let me know or directly open a new issue on gitlab.
